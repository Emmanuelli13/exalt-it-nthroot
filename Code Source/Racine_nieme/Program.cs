﻿using System;

namespace Racine_nieme
{
    class Program
    {
        static void Main(string[] args)
        {
            double d = 0d;
            d = GetNthRoot(25, 3);
            Console.WriteLine(d);
            


        }

        public static double GetNthRoot(double number, int n)
        {
            double result = 1;
            double x = 0.1d;
            try
            {
                while (Abs(result) > 0)
                {
                    result = (number / PowFunct(x, n - 1) - x) / n;
                    x += result;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            return x;
        }

        public static double PowFunct(double a, int b)
        {
            try
            {
                for (int i = 0; i < b - 1; i++)
                {
                    a *= a;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
           
            return a;
        }

        public static double Abs(double a)
        {
            return (a <= 0) ? - a : a;
        }
    }
}
