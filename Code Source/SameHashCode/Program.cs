﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SameHashCode
{
    class Program
    {
        static void Main(string[] args)
        {
            Dictionary<int, string> dict = GenerateString(2);
            Console.WriteLine(dict);
        }

        static public Dictionary<int, string> GenerateString(int number)
        {
            var rString = new Dictionary<int, string>();

            Random res = new Random();
            String str = "abcdefghijklmnopqrstuvwxyz";
            int size = 6;
            String ran = "";
            for (int y = 0; y < number; y++)
            {
                for (int i = 0; i < size; i++)
                {
                    int x = res.Next(26);

                    // Appending the character at the 
                    // index to the random string.
                    ran = ran + str[x];
                }
                rString.Add(y, ran);
            }
            return rString;
        }
    }
}
